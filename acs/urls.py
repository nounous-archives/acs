from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import cwmp.urls

urlpatterns = patterns('',
    url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/cwmp/'}),
    url(r'^cwmp', include(cwmp.urls)),
    url(r'^hook', cwmp.views.hook),
    # Examples:
    # url(r'^$', 'acs.views.home', name='home'),
    # url(r'^acs/', include('acs.foo.urls')),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

