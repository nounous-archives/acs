#!/bin/sh
# Modified version implementing InternetGatewayDevice.LANDevice.* parameters of
# TR-069 data-model
# This version is -for now- read-only
# Supported: 
# * Multiple network interfaces with protocols:
#   * bride
#   * static addressing
#   * dhcp
# * multiple bssid (different wireless networks)
# * Associated clients mac addresses
# To put on /usr/share/freecwmp/functions/lan_device on the AP (CPE)
# Copyright (C) 2013 Daniel Stan <daniel.stan@crans.org>

# Debug purpose:
DEBUG=yes
freecwmp_output () {
  local param=$1
  shift
  echo "('$param', '$*'),"
}


match() {
  # Basic function telling if an argument is matching the current queried path
  [ "$1" = "_all" ] || [ "$1" = "$2" ]
  return $?
}

uci_count_list () {
  # Print number of items of the form $1[i] in uci
  local c=0
  while true; do
    uci -q get "$1[$c]" > /dev/null || {
      echo $c
      return 0
    }
    c=$(($c+1))
  done;
}

get_iface_mac() {
  # print mac's interface
  if [ -z $1 ]; then echo ""
    else ifconfig $1 2> /dev/null | grep -oEi "([0-9a-f]{2}:){5}[0-9a-f]{2}" \
                                  | tr 'A-F' 'a-f'
  fi  
}

get_iface_status() {
  case "`ip l show $1 | grep -o 'state [^ ]*' `" in
    "state UP ")
      echo Up;;
    *)
      echo Disabled;;
  esac
}

uci_iter_list () {
  local path=$1
  local fct=$2
  local wanted=$3
  shift 3
  [ "$wanted" = "_all" ] || {
    $fct $wanted "$@"
    return
  }
  local c=0
  while true; do
    uci -q get $path[$c] > /dev/null || break
    $fct $(($c+1)) $@
    c=$(($c+1))
  done
}

do_stat() {
  # do_stat $LEI.Stats.BytesSent "TX bytes" $ifacename
  freecwmp_output $1 `ifconfig $3 | sed "s/^.*$2:\([0-9]*\) .*$/\1/; t; d"`
}

get_LANDevice() {
  local netid=$(($1-1))
  shift 
  # Local params
  # Todo: more than one EthernetInterface (eg bridge interface)
  match $1 "LANEthernetInterfaceNumberOfEntries" && {
    freecwmp_output "InternetGatewayDevice.LANDevice.$(($netid+1)).LANEthernetInterfaceNumberOfEntries" "1"
  }
  # Consider it as non implemented
  match $1 "LANUSBInterfaceNumberOfEntries" && {
    freecwmp_output "InternetGatewayDevice.LANDevice.$(($netid+1)).LANUSBInterfaceNumberOfEntries" "0"
  }
  match $1 "LANWLANConfigurationNumberOfEntries" && {
    # TODO Correct here val
    freecwmp_output "InternetGatewayDevice.LANDevice.$(($netid+1)).LANWLANConfigurationNumberOfEntries" "0"
  }
  match $1 "LANHostConfigManagement" && {
    # No support for dhcp & co
    # only one (or zero) IP config
    [ "`uci get network.@interface[$netid].proto`" = none ] && local num=0 || local num=1
    local LHCM="InternetGatewayDevice.LANDevice.$(($netid+1)).LANHostConfigManagement"
    match $2 IPInterfaceNumberOfEntries && {
      freecwmp_output "$LHCM.IPInterfaceNumberOfEntries" $num
    }
    [ $num -gt 0 ] && match $2 "IPInterface" && {
      match $3 "1" && {
        match $4 "Enable" && {
          freecwmp_output "$LHCM.IPInterface.1.Enable" 1
        }
        local proto=`uci get network.@interface[$netid].proto`
        match $4 "IPInterfaceAddressingType" && {
          case $proto in 
            static)
              freecwmp_output "$LHCM.IPInterface.1.IPInterfaceAddressingType" "Static";;
            dhcp)
              freecwmp_output "$LHCM.IPInterface.1.IPInterfaceAddressingType" "DHCP";;
            none)
              true;; #No ip here
            *) # Something else ?
              #freecwmp_output "$LHCM.IPInterface.1.IPInterfaceAddressingType" "AutoIP";;
              true;;
           esac
         }
         case `uci get network.@interface[$netid].type -q` in
           bridge)
             local iface=br-`uci show network.@interface[$netid] | \
                        sed "s/^network\.\([^\.]*\)=.*$/\1/; t; d"`;;
           *)
             local iface=`uci get network.@interface[$netid].ifname`
             ;;
         esac
         match $4 "IPInterfaceIPAddress" && {
            local ip=`ifconfig $iface | sed "s/^.*addr:\([^ ]*\) .*$/\1/; t; d"`
            [ -n "$ip" ] && freecwmp_output "$LHCM.IPInterface.1.IPInterfaceIPAddress" $ip
         }
         match $4 "IPInterfaceSubnetMask" && {
           local mask=`ifconfig $iface | sed "s/^.*Mask:\([^ ]*\).*$/\1/; t; d"`
           [ -n "$mask" ] && freecwmp_output "$LHCM.IPInterface.1.IPInterfaceSubnetMask" $mask
         }
      }
    }
  }
  #sub-objects
  local ifacetype=$1
  local ifacenum=$2
  shift 2
  match $ifacetype "LANEthernetInterfaceConfig" && \
  match $ifacenum "1" && {
    local ifacename=`uci -q get network.@interface[$netid].ifname`
    local LEI=InternetGatewayDevice.LANDevice.$(($netid+1)).LANEthernetInterfaceConfig.1
    match $1 Enable && {
      freecwmp_output $LEI.Enable \
      	"`get_iface_status $ifacename`"
    }
    match $1 MACAddress && {
      freecwmp_output $LEI.MACAddress \
        "`get_iface_mac $ifacename`"
    }
    match $1 Stats && {
      do_stat $LEI.Stats.PacketsReceived "RX packets" $ifacename
      do_stat $LEI.Stats.PacketsSent "TX packets" $ifacename
      do_stat $LEI.Stats.BytesReceived "RX bytes" $ifacename
      do_stat $LEI.Stats.BytesSent "TX bytes" $ifacename
    }
  }
  match $ifacetype "WLANConfiguration" && {
    # Something else when it is not a bridge
    # look for first wifi-iface that has a bridge in it
    # for now only single radio device config is supported
    local iface=`uci show network.@interface[$netid] | sed "s/^network\.\([^\.]*\)=.*$/\1/; t; d"`
    local c=0
    local matched=1
    local wiface=
    while true; do
      wiface="`uci -q get wireless.@wifi-iface[$c].network`" 
      [ -z "$wiface" ] && break
      # Should be $wiface in $iface
      [ "$wiface" = "$iface" ] && {
        # print output if asked
        match $ifacenum $matched && {
          get_wireless_device "InternetGatewayDevice.LANDevice.$(($netid+1)).WLANConfiguration.$matched" $c "$@"
        }
        matched=$(($matched+1))
      }
      c=$(($c+1))
    done
  }
}

get_wireless_device() {
  local prefix=$1
  local wiface_entry=$2
  shift 2
  # Todo: wifaces are shifted when one is disabled
  local wiface=wlan0
  # If not first interface, it is a bssid
  [ $wiface_entry -ne 0 ] && wiface=$wiface-$wiface_entry
  match $1 SSID && {
    freecwmp_output "$prefix.SSID"\
            `uci get -q wireless.@wifi-iface[$wiface_entry].ssid`
  }
  match $1 BSSID && {
    local v=`get_iface_mac $wiface`
    [ -n "$v" ] && freecwmp_output "$prefix.BSSID" $v
  }
  match $1 Enable && {
    [ "`uci get -q wireless.@wifi-iface[$wiface_entry].disabled`" = 1 ] && \
      freecwmp_output "$prefix.Enable" 0 || \
      freecwmp_output "$prefix.Enable" 1
  }
  # Todo: lots of stuff to still implement
  match $1 TotalAssociations && {
    # Don't crash if there's nothing to do (TODO)
    freecwmp_output "$prefix.TotalAssociations" `iw $wiface station dump | grep ^Station | wc -l`
  }
  match $1 AssociatedDevice && {
    get_assoc $prefix.AssociatedDevice $wiface $@
  }
}

get_assoc() {
  local prefix=$1
  local wiface=$2
  local wanted_cl=$4
  shift 4
  local c=1
  while true; do
    local val=`iw $wiface station dump | sed 's/^Station \([^ ]*\) .*$/\1/; t; d' | sed "$c!d"`
    [ -z "$val" ] && break
    match $1 $c && {
      freecwmp_output $prefix.1.AssociatedDeviceMACAddress $val
    }
    c=$(($c+1))
  done
}


process() {
  match $1 "InternetGatewayDevice" && {  
  shift 1
    match $1 "LANDeviceNumberOfEntries" && {
      freecwmp_output "InternetGatewayDevice.LANDeviceNumberOfEntries" "`uci_count_list network.@interface`"
    }
    match $1 "LANDevice" && {
      shift 1
      [ -z $1 ] || \
          uci_iter_list network.@interface get_LANDevice "$@"
    }
  }
}

get_lan_device() {
  # TODO: max-depth ?
  process `echo "$1" | sed 's/\.$/ _all _all _all _all _all _all _all/; s/\./ /g'`
}

[ -n "$DEBUG" ] && get_lan_device $1

set_lan_device() {
  # for now, no settings
  true
}
