#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import xml.dom.minidom

with open('tr-098-1-2-1.xml', 'r') as f:
#with open('tr-069-1-0-0.xml', 'r') as f:
    doc = xml.dom.minidom.parse(f)

def enum_tags(node, res):
    for ch in node.childNodes:
        if ch.nodeType == ch.ELEMENT_NODE:
            res.add(ch.tagName)
            enum_tags(ch, res)

res = set()
for x in doc.getElementsByTagName('syntax'):
    enum_tags(x, res)

print res
