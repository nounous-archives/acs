# -*- encoding: utf-8 -*-
from django.db import models
import datetime
import time
import string
import random
import common
import xmlutils

import acs.settings as settings
from django.utils.timezone import utc

class CPE(models.Model):
    """Un objet Customer Premises Equipment.
       
       En gros, une borne Wifi.
    """

    last_update = models.DateTimeField(auto_now=True)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    def update_parameter_list(self, liste):
        for name, value in liste:
            print "updating %s" % name
            param = Parameter.objects.get_or_create(cpe=self,
                        name=name)[0]
            param.value = value
            param.save()

    def bootstrap(self):
        new_username = 'promethee'
        new_password = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10))
        PendingChange(cpe=self, new_value=new_username,
                      name='InternetGatewayDevice.ManagementServer.Username', type=common.CHANGES_SET).save()
        PendingChange(cpe=self, new_value=new_password,
                      name='InternetGatewayDevice.ManagementServer.Password', type=common.CHANGES_SET).save()
        PendingChange(cpe=self, new_value=new_username,
                      name='InternetGatewayDevice.DeviceInfo.SerialNumber', type=common.CHANGES_SET).save()
        
        self.username = new_username
        self.password = new_password
        self.save()
    
    def get_parameters(self):
        return Parameter.objects.all().filter(cpe__exact=self)

    def __unicode__(self):
        return u"%s (%d)" % (unicode(self.username), self.pk)

class Parameter(models.Model):
    """Une propriété d'un CPE"""

    last_update = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=xmlutils.PARAMETERS_MAX_LENGTH)
    value = models.TextField(null=True)

    # Todo: tuple (cpe,name) should be globally unique
    cpe = models.ForeignKey(CPE)

    class Meta:
        unique_together = ('cpe', 'name')

    def __unicode__(self):
        return u"%s: %s = %s" % (unicode(self.cpe), self.name, self.value)
    
    def description(self):
        """Retourne la description du Parameter"""
        return xmlutils.USED_DATA_MODEL.get_parameter(self.name)["description"]

class PendingChange(models.Model):
    """Une propriété à mettre à jour, ou une action à effectuer"""

    name = models.CharField(max_length=xmlutils.PARAMETERS_MAX_LENGTH)
    new_value = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    done = models.BooleanField(default=False)
    type = models.CharField(max_length=16,
                                      choices=common.CHANGES_TYPES,
                                      default=common.CHANGES_TYPES_DEFAULT)
    cpe = models.ForeignKey(CPE)
    
    def __unicode__(self):
        return u"%s %s for %s" % (self.type,self.name,unicode(self.cpe))


LAST_SET={}

class WantedParameter(models.Model):
    """Attributs dont on souhaite maintenir"""
    name = models.CharField(max_length=xmlutils.PARAMETERS_MAX_LENGTH)
    delay = models.IntegerField(default=300)
    type = models.CharField(max_length=16,
                                      choices=common.CHANGES_TYPES,
                                      default=common.CHANGES_TYPES_DEFAULT)
    value = models.TextField(null=True)

    def __unicode__(self):
        hours, remainder = divmod(self.delay, 3600)
        minutes, seconds = divmod(remainder, 60)
        date = "%ss" % seconds
        if hours > 0 : date = "%sh %smin %s" % (hours, minutes, date)
        elif minutes > 0 : date = "%smin %s" % (minutes, date)
        str = u"%s (%s)" % (unicode(self.name), date)
        if self.type == common.CHANGES_SET:
            str = "%s = %s" % (str,self.value)
        return str
        

    @classmethod
    def set_lazy_parameter_list(self, cpe):
        liste = self.objects.all().filter(type__exact=common.CHANGES_SET)
        cpe_param=[ (param.name, param.value) for param in cpe.get_parameters() ]
        param_list = []
        for param in liste:
            if not (param.name,param.value) in cpe_param:
                if not param.name in LAST_SET.keys():
                    LAST_SET[param.name]={}
                if (time.time() - LAST_SET[param.name].get(cpe.pk,0)) > param.delay:
                    param_list.append((param.name,param.value))
                    LAST_SET[param.name][cpe.pk] = time.time()
            else:
                try: del(LAST_SET[param.name][cpe.pk])
                except: pass
        return param_list

    @classmethod
    def get_lazy_name_list(self, cpe):
        """
        Retourne les attributs à retrieve car plus à jour, pour un CPE donné
        """
        liste = self.objects.all().filter(models.Q(type__exact=common.CHANGES_GET) | models.Q(type__exact=common.CHANGES_SET) )
        current = datetime.datetime.utcnow().replace(tzinfo=utc)
        return [ param.name for param in liste \
          if len(Parameter.objects.all().filter(
             cpe__exact=cpe, name__exact=param.name, 
             last_update__gte=current - datetime.timedelta(seconds=param.delay))) == 0
               ]


