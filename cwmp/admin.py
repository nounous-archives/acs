from django.contrib import admin
from models import CPE, Parameter, WantedParameter, PendingChange

admin.site.register(CPE)
admin.site.register(Parameter)
admin.site.register(PendingChange)
admin.site.register(WantedParameter)
