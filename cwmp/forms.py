# -*- coding: utf-8 -*-

from django.forms import Form
import django.forms.models as dfm

from django import forms
import models

def fallback_to_charfield(field):
    if isinstance(field, forms.TextInput):# and field.name == 'target_field_name':
        return forms.CharField(label='Sample Label')
    return field.formfield()

EditCPEForm = dfm.inlineformset_factory(models.CPE, models.Parameter, extra=0,
                   can_delete=False, formfield_callback=fallback_to_charfield)
