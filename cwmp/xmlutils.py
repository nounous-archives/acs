#!/usr/bin/env python
# -*- encoding: utf-8 -*-


from lxml import etree

import acs.settings as settings

DATA_MODELS = {}

PARAMETERS = []


def get_all_parameter(xml):
    name=set()
    object_list=xml.xpath('//object')
    for object in object_list:
        param_list=object.xpath('.//parameter')
        keyo=object.attrib.keys()
        for param in param_list:
            try:
                name.add("%s%s" % (
                        object.attrib.get('base',
                            object.attrib.get('name',
                                object.attrib.get('ref')
                            )
                        ),
                        param.attrib.get('base',
                            param.attrib.get('name',
                                param.attrib.get('ref')
                            )
                        )
                )
                )
            except:
                print object.attrib
                raise
    return name
    
def parse_model(data_model_filename):
    """Récupère l'objet xml du data model en le lisant dans le fichier."""
    model_file = open(settings.DATAMODELS_DIR + data_model_filename)
    model = etree.parse(model_file)
    return model

def get_model(data_model_filename):
    """Renvoie l'objet xml du data model."""
    if DATA_MODELS.has_key(data_model_filename):
        return DATA_MODELS[data_model_filename]
    else:
        print "Parsing XML data model %s" % (data_model_filename)
        model = parse_model(data_model_filename)
        DATA_MODELS[data_model_filename] = model
        return model

PARAMETERS = list(
        get_all_parameter(
            get_model('tr-098-1-0-0.xml')
        ).union(
            get_all_parameter(
                get_model('tr-069-1-0.xml')
            )
        )
    )
PARAMETERS.sort()
PARAMETERS_MAX_LENGTH = max([ len(param) for param in PARAMETERS])


class XMLDataModel(object):
    """Un Modèle XML"""
    _default_data = {"description" : '', "syntax" : None, "access" : "readOnly"}
    def _get_default_data(self, fill_empty=True):
        """Renvoie une copie des valeurs par défaut, ou un dico vide si ``fill_empty`` est ``False``"""
        if fill_empty:
            return dict(self._default_data)
        else:
            return {}
    
    def __init__(self, data_model_filename):
        self.filename = data_model_filename
        self.model = get_model(data_model_filename)
        self.name = self.model.find("model").attrib["name"]
    
    def _extract_info(self, xmlobject):
        """Extrait de l'objet les données description, syntax, access."""
        extracted = {}
        description = xmlobject.find("description")
        if description is not None:
            extracted["description"] = description.text
        syntax = xmlobject.find("syntax")
        if syntax is not None:
            extracted["syntax"] = syntax
        access = xmlobject.attrib.get("access", None)
        if access is not None:
            extracted["access"] = access
        return extracted
    
    def get_something(self, something, typ, fill_empty=True):
        """Récupère la description de quelque chose.
           Si ``fill_empty`` est ``False``, le dico résultat ne fallback pas sur les valeurs par défaut."""
        model = self.model
        l = model.xpath("//%s[@name='%s']" % (typ, something))
        if l:
            # On prend les valeurs par défaut
            dico = self._get_default_data(fill_empty)
            extracted = self._extract_info(l[0])
            # On écrase avec les extraites
            dico.update(extracted)
            return dico
        else:
            # Si on n'a pas trouvé, il est possible que l'objet soit importé d'un autre data_model
            # on cherche un match avec base= à la place de name=
            somethings = model.xpath("//%s[@base='%s']" % (typ, something))
            if somethings:
                xml_something = somethings[0] 
                description = xml_something.find("description")
                action = ""
                if description is not None:
                    action = description.attrib.get("action", 'replace')
                # Il faut récupérer l'objet qui est surchargé
                # On cherche le nom du modèle qu'on est en train de surcharger
                parent_data_model_name = model.find("model").attrib["base"]
                # On va récupérer le nom du fichier contenant ce modèle
                model_imported = model.xpath("//model[@name='%s']" % (parent_data_model_name,))[0]
                importer = model_imported.getparent()
                parent_data_model_filename = importer.attrib["file"]
                parent_data_model = XMLDataModel(parent_data_model_filename)
                parent_dict = parent_data_model.get_something(something, typ)
                # On prend le dico vide
                dico = self._get_default_data(False)
                # On le remplit avec les données les plus anciennes
                dico.update(parent_dict)
                if action == "append":
                    old_description = dico["description"]
                # On écrase avec les données surchargées
                dico.update(self._extract_info(xml_something))
                if action == "append":
                    dico["description"] = old_description + dico["description"]
                # on remplace la balise {{param}} par le nom de l'objet/paramètre
                dico["description"] = dico["description"].replace("{{param}}", something)
                return dico
            else:
                return {"access" : "AccessNotFound", "description" : "DescriptionNotFound", "syntax" : None}
    
    def get_parameter(self, param):
        """Récupère la description d'un parameter."""
        # On récupère le nom du paramètre qui est à la fin du .-separeted string
        param_name = param.split(".")[-1]
        return self.get_something(param_name, "parameter")
    
    def get_object(self, obj):
        """Récupère la description d'un objet."""
        return self.get_something(obj, "object")

USED_DATA_MODEL = XMLDataModel(settings.USED_DATA_MODEL_FILENAME)
