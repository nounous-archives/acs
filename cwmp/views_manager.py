# -*- encoding: utf-8 -*-

from django.http import HttpResponse

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from lxml import etree

import common
import models
import forms
import xmlutils

import collections
import re


def affiche(request):
    """Affichage des CPEs"""
    cpes_list = models.CPE.objects.all()
    return render(request, 'cwmp/liste_ap.html',
        { 'cpes_list': cpes_list,
        })


def edit(request, pk=None):
    """Page d'édition des paramètres d'un CPE"""
    print "edit page"
    if request.method == "GET":
        cpe = models.CPE.objects.get(pk=pk)
        formset = forms.EditCPEForm(instance=cpe)
        return render(request, 'cwmp/edit_cpe.html', {"cpe" : cpe, "form" : formset})
    else:
        print request.POST
        pass

def home(request):
    root='/cwmp'
    cpe_list = models.CPE.objects.all()
    return render(request, 'cwmp/home.html', { 'cpe_list': cpe_list, 'root': root})
    
def rpc_call(request, pk=None, rpc_method=None, parameter_name=None, parameter_value=None):
    if rpc_method in [ method[0] for method in common.CHANGES_TYPES ] and pk:
        cpe=models.CPE.objects.get(pk=pk)
        print "%s %s %s %s" % (unicode(cpe),rpc_method,parameter_name, parameter_value)
        models.PendingChange(cpe=cpe, new_value=parameter_value,
                      name=parameter_name, type=rpc_method).save()
    res=HttpResponse(status=302)
    res['Location']='/cwmp/cpe/%s/' % pk
    return res
    
def cpe(request, pk=None, template='cwmp/cpe.html', **params):
    root='/cwmp'
    cpe_list = models.CPE.objects.all()
    menu_list=list(common.CHANGES_TYPES)
    if pk:
        cpe = models.CPE.objects.get(pk=pk)
        object_list=tree_group(cpe.get_parameters())
        dict={'cpe_list': cpe_list, 'root': root, 'menu_list':menu_list, 'cpe':cpe, 'object_list':object_list, 'GET':common.CHANGES_GET}
        dict.update(params)
        return render(request, template, dict)

def set(request, pk=None):
    return cpe(request, pk, template='cwmp/set.html', PARAMETERS=xmlutils.PARAMETERS)

def possible_parameters(request):
    root='/cwmp'
    cpe_list = models.CPE.objects.all()
    return render(request, 'cwmp/possible_parameters.html', {'cpe_list': cpe_list, 'root': root, 'PARAMETERS': xmlutils.PARAMETERS }) 

class PrettyObject(object):
    name = ''
    value = None

    def __init__(self, name, value, sub_values):
        self.parameters = []
        self.objects = []
        self.name = name
        self.value = value
        for v in sub_values:
            if v.value:
                self.parameters.append(v)
            else:
                self.objects.append(v)

def _tree_group(liste):
    """liste est une liste de (name, param) où name est le path absolu du
       paramètre et param une instance de paramètre (ou autre chose)
       La fonction renvoie une liste de PrettyObject
    """
    parameters_by_prefix = collections.defaultdict(list)
    direct_values = []
    for name, value in liste:
        path = name.split('.', 1)
        if len(path) == 1:
            direct_values.append(PrettyObject(path[0],value,[]))
        else:
            parameters_by_prefix[path[0]].append((path[1],value))
    res = direct_values
    for (key, sublist) in parameters_by_prefix.iteritems():
        res.append(PrettyObject(key, None, _tree_group(sublist)))
    return res

def tree_group(params):
    return _tree_group((re.sub('\.([0-9]+)',' \\1', p.name), p) for p in params\
                                                                if p.value )
