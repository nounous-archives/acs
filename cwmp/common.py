# -*- coding: utf-8 -*-

DEFAULT_USER = 'crans'
DEFAULT_PASS = 'plopisnotsecure'

NS = {
  'soap_env' : 'http://schemas.xmlsoap.org/soap/envelope/',
  'soap'     : 'http://schemas.xmlsoap.org/soap/encoding/',
  'xsd'      : 'http://www.w3.org/2001/XMLSchema',
  'xsi'      : 'http://www.w3.org/2001/XMLSchema-instance',
  'cwmp'     : 'urn:dslforum-org:cwmp-1-2',
  'dm'       : 'urn:broadband-forum-org:cwmp:datamodel-1-0',
  'dmr'      : 'urn:broadband-forum-org:cwmp:datamodel-report-0-1',
}

RPC_METHODS = [
        'GetRPCMethods',
        'Inform',
        'TransferComplete',
    ]
    
CHANGES_SET = 'SET'
CHANGES_GET = 'GET'
CHANGES_REBOOT = 'REBOOT'
CHANGES_DOWNLOAD = 'DOWNLOAD'
CHANGES_FACTORY_RESET = 'FACTORY_RESET'
CHANGES_TYPES = (
    (CHANGES_SET, 'Set a value'),
    (CHANGES_GET, 'Get a value'),
    (CHANGES_REBOOT, 'Reboot'),
    (CHANGES_DOWNLOAD , 'Download a file'),
    (CHANGES_FACTORY_RESET, 'Do a factory reset')
)
CHANGES_TYPES_DEFAULT = CHANGES_SET


