# -*- encoding: utf-8 -*-
# 
# Copyright (C) 2013 Valentin Samir <valentin.samir@crans.org>
# Copyright (C) 2013 Daniel Stan <daniel.stan@crans.org>
# Copyright (C) 2013 Vincent Legallic <vincent.legallic@crans.org>


from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from lxml import etree

from dispatch import auth
import common
import models

import xmlutils

cwmp_parser = etree.XMLParser(
        schema=etree.XMLSchema(etree.parse('ressources/schemas/cwmp-1-2.xsd')), # vérification du type
        remove_blank_text=True, # On supprime les espaces sinon freecwmp segfault
    )
    
cwmp_parser_no_validation = etree.XMLParser(
        remove_blank_text=True, # On supprime les espaces sinon freecwmp segfault
    )



def __rpc_call(request, name, **params):
    """Méthode par défaut pour générer un xml de réponse.
    On vérifie le type du xml produit avant de l'envoyer et on nétoie les espaces"""
    print "Message type send: %s" % name
    res = render(request,'cwmp/' + name + '.xml', params)
    # pretty_print=True sinon freecwmp ne comprend pas le xml
    try:
        res.content=etree.tostring(etree.fromstring(res.content, cwmp_parser), pretty_print=True)
        return res
    except etree.XMLSyntaxError as str:
        print "Erreur de typage XML : \n %s" % str
        return HttpResponse(status=204)

##### Requêtes de l'ACS au CPE #####

def set_parameter_values(request, parameter_list, parameter_key):
    """Modifie la valeur de paramètres du CPE"""
    return __rpc_call(request, "SetParameterValues", parameter_list=parameter_list, parameter_key=parameter_key)

def get_paramater_values(request, parameter_names):
    """Récupère la valeur de paramètres du CPE"""
    return __rpc_call(request, "GetParameterValues", parameters_names=parameter_names)

def reboot(request, command_key):
    """Redémarre le CPE"""
    return __rpc_call(request, "Reboot", command_key=command_key)
    
def download(request, url, file_type, file_size, target, delay,
    success_url, failure_url, username, password, command_key):
    """Télécharge un fichier sur le CPE"""
    return __rpc_call(request, "Download", url=url, file_type=file_type,
        file_size=file_size, target=target, delay=delay, success_url=success_url,
        failure_url=failure_url, username=username, password=password,
        command_key=command_key)
        
def factory_reset(request):
    """Effectue un 'factory reset' sur le CPE"""
    return __rpc_call(request, "FactoryReset")


##### Réponse de l'ACS au CPE #####

def inform_response(request):
    return __rpc_call(request, "InformResponse")
    
def get_rcp_method_response(request):
    return __rpc_call(request, "GetRPCMethodsResponse", rpc_method_list=common.RPC_METHODS)
    
def transfer_complete_response(request):
    return __rpc_call(request, "TransferCompleteResponse", rpc_method_list=common.RPC_METHODS)



##### Traitement des requêtes #####

def get_parameter_list(xml):
    """ Récupère la liste des paramètres dans un arbre XML"""
    #~ lists=xml.xpath('/soap_env:Envelope/soap_env:Body/*/ParameterList', namespaces=common.NS)
    lists=xml.xpath('//ParameterValueStruct', namespaces=common.NS)
    return [ (param.getchildren()[0].text,param.getchildren()[1].text) for param in lists]
    #~ parameter_list=[]
    #~ for list in lists:
        #~ parameter_list.extend([
            #~ (param.getchildren()[0].text,
            #~ param.getchildren()[1].text) 
            #~ for param in list])
    #~ return parameter_list


def get_message(body):
    """ Renvoie un tuple (type du message, arbre xml depuis la racine),
        le type message étant une chaîne désignant une méthode RPC
        l'arbre est bien typé s'il existe (depuis la racine) mais peut valoir
        None.
        body est une chaîne de caractères
        """
    if len(body)==0:
        return 'Empty',None
    try:
        xml = etree.fromstring(body, cwmp_parser_no_validation)
    except etree.XMLSyntaxError as str:
        print body
        print "Erreur de typage XML : \n %s" % str
        return 'Error', None
    message_type=xml.xpath('/soap_env:Envelope/soap_env:Body/*/.', namespaces=common.NS)[0]
    return message_type.tag.replace('{%s}' % common.NS['cwmp'],''),xml



def pending_changes(request, cpe):
    """ Envois au CPE ce qu'il y a à faire """
    # Pending SET
    pending_change = models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_SET)
    set_parameters = models.WantedParameter.set_lazy_parameter_list(cpe)
    if len(pending_change) != 0 or  len(set_parameters) !=0:
        set_parameters.extend([ (param.name, param.new_value) for param in pending_change ])
        request.session['set_parameter'] = [param.pk for param in pending_change ]
        return set_parameter_values(request, set_parameters, '')
        
    # Pending DOWNLOAD
    pending_change = models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_DOWNLOAD)
    if len(pending_change) != 0:
        download=pending_change[0]
        # On suppose que les paramètres sont dans le champs new_value avec des : comme séparateur
        (url, file_type, file_size, target, delay, success_url, 
            failure_url, username, password) = download.new_value.split(':')
        command_key = download.key
        request.session['download'] = [ download.pk ]
        return download (request, url=url, file_type=file_type, 
            file_size=file_size, target=target, delay=delay, 
            success_url=success_url, failure_url=failure_url, 
            username=username, password=password, command_key=command_key)
    
    # Pending FACTORY_RESET
    # pending_change = models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_FACTORY_RESET)
    # if len(pending_change) != 0:
    #    request.session['factory_reset'] = [param.pk for param in pending_change ]
    #    return factory_reset(request)

    # Pending REBOOT
    pending_change = models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_REBOOT)
    if len(pending_change) != 0:
        request.session['reboot'] = [param.pk for param in pending_change ]
        return reboot(request, pending_change[0].pk)

    # Pending GET
    pending_change = models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_GET)
    get_parameters = models.WantedParameter.get_lazy_name_list(cpe)
    get_parameters.extend([ param.name for param in pending_change])
    if len(get_parameters) != 0:
        request.session['get_parameter'] = [param.pk for param in pending_change ]
        return get_paramater_values(request, get_parameters)

    print "Nothing to do, closing session"
    return HttpResponse(status=204)


# On n'utilise pas la protection contre le cross side scripting ici
# (il y a une auth http basique de toute façon)
@csrf_exempt
def hook(request):
    global get_parameter_list
    print "received from server: (size %d)" % (len(request.body))
    res = None

    try:
        cpe = auth(request)
    except models.CPE.DoesNotExist:
        print "cpe using wrong credentials"
        res=HttpResponse(status=401)
        res['WWW-Authenticate'] = 'Basic realm="ACS"'
        return res
        
    # On détermine le type du message et on le parse
    (message_type,xml)=get_message(request.body)
    print "Message type received: %s" % message_type
    
    
    
    ### Requêtes du CPE ###
    
    if message_type == 'Inform':
        # Màj des données
        cpe.update_parameter_list(get_parameter_list(xml))
        res = inform_response(request)
        
    elif message_type == 'GetRPCMethods':
        res = get_rcp_method_response(request)
        
    elif message_type == 'TransferComplete':
        node=xml.xpath('/soap_env:Envelope/soap_env:Body/cwmp:TransferComplete', namespaces=common.NS)[0]
        command_key = node.xpath('/CommandKey/text()', namespaces=common.NS)
        start_time = node.xpath('/StartTime/text()', namespaces=common.NS)
        complete_time = node.xpath('/CompleteTime/text()', namespaces=common.NS)
        fault_code = node.xpath('/FaultStruct/FaultCode/text()', namespaces=common.NS)
        fault_string = node.xpath('/FaultStruct/FaultString/text()', namespaces=common.NS)
        res = transfer_complete_response(request)
        
        
        
    ### Réponse du CPE à des requêtes de l'ACS ###
        
    elif message_type == 'GetParameterValuesResponse':
        print get_parameter_list(xml)
        print etree.tostring(xml, pretty_print=True)
        cpe.update_parameter_list(get_parameter_list(xml))
        models.PendingChange.objects.all().filter(cpe__exact=cpe,pk__in=request.session['get_parameter']).delete()
        
    elif message_type == 'GetRPCMethodsResponse':
        rpc_methods = xml.xpath('/soap_env:Envelope/soap_env:Body/' + 
            'cwmp:GetRPCMethodsResponse/MethodList/xsd:string/text()', namespaces=common.NS)
        
    elif message_type == 'SetParameterValuesResponse':
        status = int(xml.xpath('/soap_env:Envelope/soap_env:Body/' + 
            'cwmp:%s/Status/text()' % message_type, namespaces=common.NS)[0])
        if status in [ 0, 1 ]:
            if status == 0: print "All Parameter changes have been validated and applied"
            elif status == 1: print "All Parameter changes have been validated and committed, but some or all are not yet applied"
            models.PendingChange.objects.all().filter(cpe__exact=cpe,pk__in=request.session['set_parameter']).delete()
       
    elif message_type  == 'DownloadResponse':
        status = int(xml.xpath('/soap_env:Envelope/soap_env:Body/' + 
            'cwmp:%s/Status/text()' % message_type, namespaces=common.NS)[0])
       
    elif message_type == 'RebootResponse':
        print "CPE rebooting"
        models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_REBOOT).delete()
        
    elif message_type == 'FactoryResetResponse':
        print "Resetting CPE to factory default"
        models.PendingChange.objects.all().filter(cpe__exact=cpe,type__exact=common.CHANGES_FACTORY_RESET).delete()
        
    
    ### Le CPE attend pour voir si on a quelque chose à lui dire ###
    
    elif message_type == 'Empty':
        pass
        
        
    ### Message inconnu de l'ACS ou erreur lors de la réception du message ###
    
    elif message_type == 'Error':
        return HttpResponse(status=204)
        
    else:
        print "Unknow message type : %s " % message_type
        return HttpResponse(status=204)
    
    if not res: res = pending_changes(request, cpe)
    res['Content-type'] = 'text/xml; coding=utf-8'
    return res
