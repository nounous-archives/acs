# -*- coding: utf-8 -*-

from models import CPE, Parameter

import common

def auth(request):
    """ Authentifie et crée éventuellement un CPE"""
    # Caveats: il faut handle les codes http pour demander des credidentials
    # en premier

    # Premièrement, on regarde si on est dans une session déjà ouverte
    if request.session.has_key('cpe_pk'):
        return CPE.objects.get(pk=request.session['cpe_pk'])

    # Sinon, auth par HTTP
    if request.META.has_key('HTTP_AUTHORIZATION'):
        authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
        if authmeth.lower() == 'basic':
            auth = auth.strip().decode('base64')
            username, password = auth.split(':', 1)
            if username == common.DEFAULT_USER and \
               password == common.DEFAULT_PASS:
                # La borne n'a jamais été configurée
                print "New CPE"
                # Routine de première conf ?
                cpe = CPE()
                # Routines de première utilisation
                cpe.save()
                cpe.bootstrap()
            else:
                cpe_list=CPE.objects.filter(username__exact=username,
                                            password__exact=password)
                if len(cpe_list)==1:
                    cpe = cpe_list[0]
                else:
                    raise CPE.DoesNotExist

            # On utilisera des sessions, après
            request.session['cpe_pk'] = cpe.pk
            cpe.save()
            return cpe

    raise CPE.DoesNotExist
