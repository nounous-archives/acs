
from django.conf.urls import patterns, include, url

import views
import views_manager
import common

urlpatterns = patterns('',
    url(r'^/$', views_manager.home),
    url(r'^/possible_parameters/$', views_manager.possible_parameters),
    url(r'^/edit/(?P<pk>[0-9]+)/$', views_manager.edit),
    url(r'^/cpe/(?P<pk>[0-9]+)/$', views_manager.cpe),
    url(r'^/cpe/(?P<pk>[0-9]+)/%s/$' % common.CHANGES_SET, views_manager.set),
    url(r'^/cpe/(?P<pk>[0-9]+)/(?P<rpc_method>[A-z0-9]+)/(?P<parameter_name>[A-z0-9.]+)/?(?P<parameter_value>.*)$', views_manager.rpc_call),
)
